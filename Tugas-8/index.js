var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

//pemanggilan fungsi readBooks
let x = 0;
let pointer = 1;
function read(waktu, buku) {
    readBooks(waktu, buku, function (sisa) {
        if (x < 3) {
            read(sisa, books[0 + pointer])
            x++;
            pointer++;
        }
    })
}

read(10000, books[0]);
