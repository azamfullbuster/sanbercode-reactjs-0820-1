var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

//menjalankan function readBooksPromise
function read() {
    readBooksPromise(10000, books[0])
        .then(function (sisaWaktu) {
            return readBooksPromise(sisaWaktu, books[1])
        })
        .then(function (sisaWaktu) {
            return readBooksPromise(sisaWaktu, books[2])
        })
        .catch(function (sisaWaktu) {
            console.log(sisaWaktu)
        });
}

read()

//arrowfunc
//1 line
// readBooksPromise(10000, books[0])
//     .then((sisaWaktu) => readBooksPromise(sisaWaktu, books[1]))
//     .then((sisaWaktu) => readBooksPromise(sisaWaktu, books[2]))
//     .then((sisaWaktu) => readBooksPromise(sisaWaktu, books[3]))
//     .catch((sisaWaktu) => console.log(sisaWaktu));

// >1line
// readBooksPromise(10000, books[0])
//     .then((sisaWaktu) => {
//         return readBooksPromise(sisaWaktu, books[1]);
//     })
//     .then((sisaWaktu) => {
//         return readBooksPromise(sisaWaktu, books[2]);
//     })
//     .then((sisaWaktu) => {
//         return readBooksPromise(sisaWaktu, books[3]);
//     })
//     .catch((sisaWaktu) => console.log(sisaWaktu));