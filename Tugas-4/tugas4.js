//soal 1
console.log("LOOPING PERTAMA");
var batas = 0;
var nomor = 2;
while (batas < 10) {
    console.log(nomor + " - " + 'I love coding')
    nomor += 2
    batas++;
}

console.log("LOOPING KEDUA");
while (batas > 0) {
    nomor -= 2
    console.log(nomor + " - " + 'I will become a frontend developer')
    batas--;
}
console.log("\n")

//soal 2
var nomor = 0;
for (var deret = 0; deret < 20; deret++) {
    nomor++;
    if (nomor % 2 == 0) {
        console.log(nomor + " - " + "Berkualitas")
    } else if (nomor % 2 != 0 && nomor % 3 == 0) {
        console.log(nomor + " - " + "I love coding")
    } else {
        console.log(nomor + " - " + "Santai")
    }
}
console.log("\n")

//soal 3
var output = "";
for (var tinggi = 0; tinggi < 8; tinggi++) {
    for (var alas = 0; alas < tinggi; alas++) {
        output += alas;
    }
    var stroutput = output.replace(/[0-9]/g, '#')
    console.log(stroutput);
    output = '';
}
console.log("\n")

//soal 4
var kalimat = "saya sangat senang belajar javascript";
var kalimatArr = kalimat.split(" ");
console.log(kalimatArr);
console.log("\n")

//soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortBuah = daftarBuah.sort();
for (var num = 0; num < sortBuah.length; num++) {
    console.log(sortBuah[num])
}