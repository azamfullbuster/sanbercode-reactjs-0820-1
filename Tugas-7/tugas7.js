//soal 1
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name() {
        return this._name;
    }

    set name(x) {
        this._name = x;
    }

    get legs() {
        return this._legs;
    }

    set legs(x) {
        this._legs = x;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }

    set cold_blooded(x) {
        this._cold_blooded = x;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Frog extends Animal {
    constructor(name, legs, cold_blooded) {
        super(name, legs, cold_blooded);
    }

    jump() {
        return ("hop hop");
    }
}

class Ape extends Animal {
    constructor(name, cold_blooded) {
        super(name, cold_blooded);
        this.legs = 2;
    }

    yell() {
        return ("Auooo");
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"

//soal 2
class Clock {
    constructor({ template }) {
        this.template = template;
        this.timer;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(timer);
    }

    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 