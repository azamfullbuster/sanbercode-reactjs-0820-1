//soal 1
const luas = (d) => {
    const pi = 3.14;
    let r = d / 2;
    let l = pi * r * r;
    return l;
}

const keliling = (d) => {
    const pi = 3.14;
    let k = pi * d;
    return k;
}

console.log(luas(14));
console.log(keliling(14));

//soal 2
let kalimat = "";

const addKata = (kata) => {
    kalimat = `${kalimat}${kata} `;
}

addKata("saya");
addKata("adalah");
addKata("seorang");
addKata("frontend");
addKata("developer");

console.log(kalimat)

//soal 3
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName() {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}

newFunction("William", "Imoh").fullName()

//soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation)

//soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)
