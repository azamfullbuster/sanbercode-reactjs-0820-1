//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var kataGabung = kataPertama + " " + kataKedua.substr(0, 1).toUpperCase() + kataKedua.substr(1, 5) + " " + kataKetiga + " " + kataKeempat.toUpperCase();
console.log(kataGabung);

//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var hasil = Number(kataPertama) + Number(kataKedua) + Number(kataKetiga) + Number(kataKeempat);
console.log("1+2+4+5 = " + hasil);

//soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substr(4, 10);
var kataKetiga = kalimat.substr(15, 3);
var kataKeempat = kalimat.substr(19, 5);
var kataKelima = kalimat.substr(25, 6);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

//soal 4
var nilai = 75;
var indeks = "";
if (nilai >= 80) {
    indeks = "A"
}
else if (nilai >= 70 && nilai < 80) {
    indeks = "B"
}
else if (nilai >= 60 && nilai < 70) {
    indeks = "C"
}
else if (nilai >= 50 && nilai < 60) {
    indeks = "D"
}
else if (nilai < 50) {
    indeks = "E"
}

console.log("Nilai = " + nilai + ", " + "Maka indeksnya : " + indeks);

//soal 5
var tanggal = 31;
var bulan = 1;
var tahun = 1999;

var strBulan = "";
switch (bulan) {
    case 1: { strBulan = "Januari"; break; }
    case 2: { strBulan = "Februari"; break; }
    case 3: { strBulan = "Maret"; break; }
    case 4: { strBulan = "April"; break; }
    case 5: { strBulan = "Mei"; break; }
    case 6: { strBulan = "Juni"; break; }
    case 7: { strBulan = "Juli"; break; }
    case 8: { strBulan = "Agustus"; break; }
    case 9: { strBulan = "September"; break; }
    case 10: { strBulan = "Oktober"; break; }
    case 11: { strBulan = "November"; break; }
    case 12: { strBulan = "Desember"; break; }
    default: {
        strBulan = "-";
        console.log('Bulan tidak terdeteksi');
    }
}

console.log(tanggal + " " + strBulan + " " + tahun)
