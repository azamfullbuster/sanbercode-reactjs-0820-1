//soal 1
function halo() {
    return ("Halo Sanbers");
}

console.log(halo()) // "Halo Sanbers!" 
console.log("\n");

//soal 2
function kalikan(num1, num2) {
    return num1 * num2;
}


var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log("\n");

//soal 3
function introduce(nama, umur, alamat, hobi) {
    return ("Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!")
}
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
console.log("\n");

//soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]
var objectDaftarPeserta = {};

objectDaftarPeserta.nama = arrayDaftarPeserta[0];
objectDaftarPeserta["jenis kelamin"] = arrayDaftarPeserta[1];
objectDaftarPeserta.hobi = arrayDaftarPeserta[2];
objectDaftarPeserta["tahun lahir"] = arrayDaftarPeserta[3];

console.log("nama = " + objectDaftarPeserta["nama"] + "\n"
    + "jenis kelamin = " + objectDaftarPeserta["jenis kelamin"] + "\n"
    + "hobi = " + objectDaftarPeserta["hobi"] + "\n"
    + "tahun lahir = " + objectDaftarPeserta["tahun lahir"] + "\n");

//soal 5
var buah = [{ nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000 },
{ nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000 },
{ nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada", harga: 10000 },
{ nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 10000 }];

buah.forEach(function (item) {
    console.log("nama : " + item.nama)
});
console.log("\n");

//soal 6
var dataFilm = [];

function objArrPush(nama, durasi, genre, tahun) {
    var film = {};
    film.nama = nama;
    film.durasi = durasi;
    film.genre = genre;
    film.tahun = tahun;

    dataFilm.push(film);
}

objArrPush("Iron Man", 128, "Action", 2010);
objArrPush("One Piece", 240, "Comedy", 1996);
objArrPush("Tali Gerandong Perjaka", 10, "Horror", 1945);

var no = 1;
dataFilm.forEach(function (item) {
    console.log(no + ". " + "nama : " + item.nama + " | " + "durasi : " + item.durasi + " | " + "genre : " + item.genre + " | " + "tahun : " + item.tahun + " | ");
    no++;
});